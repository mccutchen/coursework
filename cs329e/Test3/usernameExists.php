<?php

require('utils.php');
    
if (isset($_GET['username'])) {
    $username = $_GET['username'];
    echo ($db->usernameExists($username)) ? '1' : '0';
} else {
    echo '0';
}