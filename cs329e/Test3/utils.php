<?php

// Show all errors by default
error_reporting(E_ALL);
ini_set('display_errors', 'true');

require('db.php');

function isBlank($s) {
    return preg_match('/^\s*$/', $s);
}

function register($username, $password) {
    global $db;
    if (isBlank($username) || isBlank($password))
        return false;
    return $db->addUser($username, $password);
}