function checkUsername() {
    var username = this.value;
    var params = { username: username };
    $.ajax('GET', 'usernameExists.php', params, function(xhr) {
        if (xhr.responseText == '1')
            alert('Username "' + username + '" is already taken.');
    });
}
    	
// Wire up the events on page load
$.onload(function() {
    var el = $('username');
    if (el)
        el.onblur = checkUsername;
});