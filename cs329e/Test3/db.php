<?php

class DB {
    private $host = 'z.cs.utexas.edu';
    private $user = 'mccutch';
    private $pass = 'stocarle';
    private $name = 'dbmccutch';
    private $table = 'RegInfo';
    
    private $con;
    
    public function __construct() {
        $this->con = new mysqli(
            $this->host, $this->user, $this->pass, $this->name
        );
        
        if (mysqli_connect_errno())
            die('mysqli_connect failed: ' . mysqli_connect_error());
    }
    
    public function addUser($username, $password) {
        if ($this->usernameExists($username))
            return false;
        $e_user = $this->escape($username);
        $e_pass = $this->escape($password);
        $sql = "INSERT INTO {$this->table} VALUES ($e_user, $e_pass);";
        return $this->con->query($sql);
    }
    
    public function usernameExists($username) {
        $sql = "SELECT * FROM {$this->table} WHERE username = {$this->escape($username)};";
        $result = $this->con->query($sql);
        return ($result->num_rows > 0);
    }
    
    private function escape($value) {
        if (is_numeric($value))
            return $value;
        else
            return "'" . $this->con->real_escape_string($value) . "'";
    }
}

// Make an instance that can be used on all pages
$db = new DB();