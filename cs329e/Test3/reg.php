<?php
    require('utils.php');
    
    $registering = ($_SERVER['REQUEST_METHOD'] == 'POST');
    $registered = false;
    
    if ($registering) {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $registered = register($username, $password);
    }
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
    	<title>User Registration</title>
    	<link rel="stylesheet" type="text/css" href="style.css" />
    	<script type="text/javascript" src="../js/utils.js"></script>
    	<script type="text/javascript" src="reg.js"></script>
    </head>
    <body>
        
        <h1>User Registration</h1>
        
        <div id="container">
            <?php if (!$registering): ?>
                <p>Fill out the form below to register.</p>
            
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                    <label>
                        Username
                        <input name="username" id="username" type="text" />
                    </label>
                    <label>
                        Password
                        <input name="password" type="password" />
                    </label>
                
                    <input type="submit" value="Submit" />
                    <input type="reset" value="Reset" />
                </form>
            <?php else: ?>
                <p class="<?php echo ($registered) ? 'success' : 'failure'; ?>">
                    Registration was <strong><?php if (!$registered) echo 'not'; ?> successful.</strong>
                </p>
            <?php endif; ?>
        </div>
    </body>
</html>
