<?php

class SignupDB implements IteratorAggregate {
    private $times;
    private $path;
    private $db;
    
    public function __construct($path) {
        $this->times = array(8, 9, 10, 11, 12, 1, 2, 3, 4, 5);
        $this->path = $path;
        $this->db = $this->read();
    }
    
    private function read() {
        $slots = explode("\n", file_get_contents($this->path));
        $db = array();
        for ($i = 0; $i < count($slots); $i++) {
            $name = $slots[$i];
            $time = $this->times[$i];
            $db[$time] = $name;
        }
        return $db;
    }
    
    public function save() {
        $a = array();
        foreach ($this->db as $time => $name) {
            $a[$this->idForTime($time)] = $name;
        }
        file_put_contents($this->path, implode("\n", $a));
    }
    
    public function set($timeId, $name) {
        $time = intval(ltrim($timeId, 't'));
        if ($this->db[$time] != '')
            return false;
        $this->db[$time] = $name;
        return true;
    }
    
    public function isFull() {
        foreach ($this->db as $time => $name) {
            if ($name == '') return false;
        }
        return true;
    }
    
    private function idForTime($time) {
        $map = array_flip($this->times);
        return $map[$time];
    }
    
    public static function timeToString($time) {
        return sprintf('%d:00 %s', $time, ($time < 12 && $time > 5) ? 'a.m.' : 'p.m.');
    }
    public static function timeToId($time) {
        return "t$time";
    }

    public function getIterator() {
        return new ArrayObject($this->db);
    }
}