<?php echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<?php
require('signupdb.php');
$db = new SignupDB('signup.txt');

$signingUp = ($_SERVER['REQUEST_METHOD'] == 'POST');
$signedUp = false;
$chosenName = null;
$chosenTime = null;

function getChosenTime() {
    foreach ($_POST as $time => $name) {
        if ($name != '') return $time;
    }
    return false;
}

if ($signingUp && !$db->isFull() && ($chosenTime = getChosenTime())) {
    $chosenName = $_POST[$chosenTime];
    if ($db->set($chosenTime, $chosenName)) {
        $signedUp = true;
        $db->save();
    }
}
?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
    	<title>Signup Sheet</title>
    	<link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        <h1>Signup Sheet</h1>
        
        <?php if ($db->isFull() && !$signedUp) { ?>
            <p>Sorry, the signup sheet is full.</p>
        <?php } elseif ($signingUp && !$signedUp && $chosenName) { ?>
            <p>Sorry, that time slot is taken.  Please choose another.</p>
        <?php } elseif ($signingUp && !$signedUp && !$chosenName) { ?>
            <p>Put your name into a time slot to sign up.</p>
        <?php } elseif ($signedUp) { ?>
            <p>Thank you for signing up!</p>
        <?php } ?>
        
        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
            <table cellspacing="0">
                <col id="time" />
                <col id="name" />
                <tr>
                    <th>Time</th>
                    <th>Name</th>
                </tr>
<?php
foreach ($db as $time => $name) {
    $id = SignupDB::timeToId($time);
    $time = SignupDB::timeToString($time);
    if ($name == '' && !$signedUp)
        $name = "<input type=\"text\" name=\"$id\" />";
    echo <<<EOT
        <tr>
            <td>$time</td>
            <td>$name</td>
        </tr>
EOT;
}
?>
            </table>
            
            <?php if (!$signedUp && !$db->isFull()) { ?>
                <p><input type="submit" value="Sign Up!" /></p>
            <?php } ?>
        </form>
    </body>
</html>
