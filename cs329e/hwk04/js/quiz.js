var answers = {
    q1: radio(match('false')),
    q2: radio(match('true')),
    q3: checkbox(match('b')),
    q4: checkbox(match('d')),
    q5: text(match(/galaxy/i)),
    q6: text(match(/age/i))
};


// Event handler called when the form is submitted. First resets the
// form, then ensures that each question is answered, and finally
// grades the quiz and reports the results to the user.
function grade() {
    reset(function() {
        for (fieldname in answers) {
            document.getElementById(fieldname).setAttribute('class', '');
        }
    });
    
    var form = document.getElementById('quizform');
    if (ensureFilled(form)) {
        validate(form);
    }
    return false;
}

// Actually grades the quiz, by running each field through its
// validator function, as defined in the global answers variable.
// Highlights any incorrect answers and reports the grade to the user.
function validate(form) {
    var incorrect = [];
    var numAnswers = 0;

    for (fieldname in answers) {
        var field = form[fieldname];
        var validator = answers[fieldname][0];
        if (!validator(field)) {
            incorrect.push(fieldname);
        }
        numAnswers++;
    }

    incorrect.each(function(name) {
        document.getElementById(name).setAttribute('class', 'error');
    });

    var correct = numAnswers - incorrect.length
    var ratio =  correct + '/' + numAnswers;
    var pct = Math.round((correct / numAnswers) * 10000) / 100;
    var msg = '<h2><span>Grade:</span> ' + ratio + ' (' + pct + '%)</h2>';
    if (incorrect.length > 0) {
        msg += '<p>Your incorrect answers are highlighted in red below.</p>';
    } else {
        msg += '<p>Nice job!</p>';
    }
    setStatus('success', msg);
}

// Ensures that each field in the form is filled.  If so, simply
// returns true.  If not, highlights each unfilled field and gives the
// user an error message before returning false.
function ensureFilled(form) {
    var unfilled = [];

    for (fieldname in answers) {
        var field = form[fieldname];
        var emptyTest = answers[fieldname][1];
        if (emptyTest(field))
            unfilled.push(fieldname);
    }

    if (unfilled.length == 0) {
        return true;
    } else {
        unfilled.each(function(name) {
            document.getElementById(name).setAttribute('class', 'error');
        });
        var msg = '<strong>Unanswered questions.</strong> Please ensure ' +
            'that you\'ve answered each question.  Those that were ' +
            'unanswered are highlighted in red below.';
        setStatus('error', msg);
        return false;
    }
}


// ====================================================================
// Validator functions. These functions are used in the answers global
// variable to establish the correct answers for the quiz.  They also
// know how to determine whether each question has been answered.
// ====================================================================
function match(s) {
    if (typeof(s) == 'string')
        return function(value) { return value == s; }
    else
        return function(value) { return value.match(s); }
}
function text(validator) {
    function validate(field) {
        return validator(field.value);
    }
    function isEmpty(field) {
        return (field.value.search(/\w+/) == -1);
    }
    return [validate, isEmpty];
}
function choice(validator) {
    function validate(field) {
        var checked = [];
        var correct = [];
        $.each(field)(function(answer) {
            if (answer.checked) {
                checked.push(answer);
                if (validator(answer.value))
                    correct.push(answer);
            }
        });
        return (correct.length == 1 && checked.length == 1);
    }
    function isEmpty(field) {
        for (var i = 0; i < field.length; i++) {
            if (field[i].checked)
                return false;
        }
        return true;
    }
    return [validate, isEmpty];
}
// radio and checkbox are just aliases of choice
function radio(validator) { return choice(validator); }
function checkbox(validator) { return choice(validator); }


// Wire up events
$.onload(function() {
    document.getElementById('submit').onclick = grade;
});
