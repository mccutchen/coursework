// Resets the form by hiding the status box and removing the error
// highlighting from each field.  If extraReset is given, it should
// be a function that will do any extra work needed.
function reset(extraReset) {
    document.getElementById('status').style.display = 'none';
    if (extraReset) extraReset();
}

// Sets the status message and displays the status box.  Type should
// be one of 'error' or 'success'.
function setStatus(type, msg) {
    var status = document.getElementById('status');
    status.setAttribute('class', type);
    status.innerHTML = msg;
    status.style.display = 'block';
    window.scrollTo(0,0);
}