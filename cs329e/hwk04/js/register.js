var errors = [];

function validate() {
    reset(function() {
        errors = [];
    });
    validateUsername(this['username'].value);
    validatePassword(this['password'].value);
    if (this['password'].value != this['repeatpassword'].value) {
        errors.push('Passwords do not match.');
    }
    
    if (errors.length == 0) {
        var msg = '<h2>Passed validation!</h2>';
        setStatus('success', msg);
    } else {
        var msg = '<p><strong>Validation errors:</strong></p><ul>';
        errors.each(function(error) {
            msg += '<li>' + error + '</li>';
        });
        msg += '</ul>';
        setStatus('error', msg);
    }
    
    return false;
}

function validateUsername(value) {
    if (!validateSize(value)) {
        errors.push('Username not between 6 and 10 characters.');
    }
    if (!validateAlphaNumeric(value)) {
        errors.push('Username not strictly letters and numbers.')
    }
    if (value.match(/^\d/)) {
        errors.push('Username starts with a digit.')
    }
}

function validatePassword(value) {
    if (!validateSize(value)) {
        errors.push('Password not between 6 and 10 characters.');
    }
    if (!validateAlphaNumeric(value)) {
        errors.push('Password not strictly letters and numbers.')
    }
    if (!value.match(/[a-z]/)) {
        errors.push('Password doesn\'t contain a lowercase letter.');
    }
    if (!value.match(/[A-Z]/)) {
        errors.push('Password doesn\'t contain an uppercase letter.');
    }
    if (!value.match(/\d/)) {
        errors.push('Password doesn\'t contain a digit.');
    }
}

function validateSize(value) {
    return (value.length >= 6 && value.length <= 10);
}
function validateAlphaNumeric(value) {
    return (value.match(/^[A-z0-9]+$/));
}


// Wire up events
$.onload(function() {
    document.getElementById('registrationform').onsubmit = validate;
});
