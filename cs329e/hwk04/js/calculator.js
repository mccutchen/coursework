var errors = [];

var results = {
    monthly: null,
    sum: null,
    total: null
};

function calculate() {
    reset(function() {
        errors = [];
        clearResults();
        window.scrollTo(0,0);
    });
    
    var principal = getPrincipal(this['principal'].value);
    var rate = getInterestRate(this['rate'].value);
    var term = getTerm(this['term'].value);
    
    if (errors.length > 0) {
        var msg = '<p><strong>Errors:</strong></p><ul>';
        errors.each(function(error) {
            msg += '<li>' + error + '</li>';
        });
        msg += '</ul>';
        setStatus('error', msg);
    } else {
        var monthlyPayment = formula(principal, rate, term);
        var totalPayments = monthlyPayment * term;
        var totalInterest = totalPayments - principal;
        showResults(monthlyPayment, totalPayments, totalInterest);
    }
    return false;
}

function formula(P, r, n) {
    return P * (r / (1 - (1/Math.pow(1+r, n))));
}

function getPrincipal(value) {
    return ensureFloat(value, 'Principal');
}
function getInterestRate(value) {
    var rate = ensureFloat(value, 'Interest rate');
    if (rate > 1)
        rate /= 100;
    return rate / 12;
}
function getTerm(value) {
    return Math.round(ensureFloat(value, 'Loan term'));
}
function ensureFloat(value, what) {
    var x = parseFloat(value);
    if (isNaN(x)) {
        errors.push(what + ' must be given as a decimal number.');
    }
    return x;
}

function dollars(amt) {
    var s = '$' + Math.round(amt * 100) / 100;
    return (s.match(/\.\d\d$/)) ? s : s + '0';
}

function showResults(monthly, sum, total) {
    results.monthly.innerHTML = dollars(monthly);
    results.sum.innerHTML = dollars(sum);
    results.total.innerHTML = dollars(total);
}
function clearResults() {
    for (id in results) {
        results[id].innerHTML = '';
    }
}


// Wire up events and initialize objects
$.onload(function() {
    document.forms[0].onsubmit = calculate;
    document.getElementById('clear').addEventListener('click', function() {
        clearResults();
        reset();
    }, false);
    
    // Initialize the results object with actual DOM elements
    for (id in results) {
        results[id] = document.getElementById(id);
    }
});