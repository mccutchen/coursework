<?php
    require_once('inc/authlib.php');
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
    	<title>Student Records</title>
    	<link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        
        <?php include('inc/userinfo.php'); ?>
        
        <h1>Student Records</h1>
        
        <div id="container">
            <h2>Menu</h2>
            <ul>
                <li><a href="view.php">View Student Records</a></li>
                <li><a href="insert.php">Insert Student Record</a></li>
                <li><a href="update.php">Update Student Record</a></li>
                <li><a href="delete.php">Delete Student Record</a></li>
            </ul>
        </div>
    </body>
</html>
