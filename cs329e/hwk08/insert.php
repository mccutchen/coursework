<?php
require_once('inc/utils.php');
require_once('inc/db.php');
require_once('inc/authlib.php');
$auth->authenticate();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $fields = $db->extractFields($_POST);
    $id = $db->escape($fields['id']);
    $last = $db->escape($fields['last']);
    $first = $db->escape($fields['first']);
    $major = $db->escape($fields['major']);
    $gpa = $db->escape($fields['gpa']);
    
    $sql = "INSERT INTO students VALUES ($id, $last, $first, $major, $gpa)";
    $result = $db->con->query($sql);
    $msg = "Added student record for {$_POST['first']} {$_POST['last']}.";
    header('Location: ./view.php?message=' . urlencode($msg));
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
    	<title>Student Records</title>
    	<link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        
        <?php include('inc/userinfo.php'); ?>
        
        <h1>Student Records</h1>
        
        <div id="container">
            <h2>Insert Student Record</h2>
            
            <p>Fill out the form below to add a student record.  All fields
            must be filled in.</p>
            
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                <table cellspacing="0">
                    <tr>
                        <th>ID</th>
                        <th>Last name</th>
                        <th>First name</th>
                        <th>Major</th>
                        <th>GPA</th>
                        <th>&nbsp;</th>
                    </tr>
                    <tr>
                        <td><input name="id" type="text" size="5" /></td>
                        <td><input name="last" type="text" /></td>
                        <td><input name="first" type="text" /></td>
                        <td><input name="major" type="text" /></td>
                        <td><input name="gpa" type="text" size="5" /></td>
                        <td><input type="submit" value="Add Record" /></td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>
