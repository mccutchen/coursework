<?php
require_once('inc/utils.php');
require_once('inc/db.php');
require_once('inc/authlib.php');
$auth->authenticate();

$error = null;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['id']) && $_POST['id'] != '') {
        $id = $_POST['id'];
        $sql = "DELETE FROM students WHERE id=$id";
        $result = $db->con->query($sql);
        $msg = "Deleted record for student id $id.";
        header('Location: ./view.php?message=' . urlencode($msg));
    } else {
        $error = 'You must enter the ID of the student whose record is to be deleted.';
    }
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
    	<title>Student Records</title>
    	<link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        
        <?php include('inc/userinfo.php'); ?>
        
        <h1>Student Records</h1>
        
        <?php if ($error): ?>
            <div id="status" class="error">
                <?php echo $error; ?>
            </div>
        <?php endif ?>
        
        <div id="container">
            <h2>Delete Student Record</h2>
            
            <p>Enter the ID of the student whose record should be deleted.</p>
            
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                <label>
                    Student ID: <input name="id" type="text" size="5" />
                </label><br />
                <input type="submit" value="Delete Record" /></td>
            </form>
        </div>
    </body>
</html>
