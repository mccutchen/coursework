<?php
require_once('inc/utils.php');
require_once('inc/db.php');
require_once('inc/authlib.php');
$auth->authenticate();

$id = '';
$error = null;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $fields = $db->extractFields($_POST);
    
    if (isset($fields['id']) && count($fields) > 1) {
        // Store the ID and remove it from the set of fields
        $id = $fields['id'];
        unset($fields['id']);
        $updates = $db->updateClause($fields);
        $sql = "UPDATE students SET $updates WHERE id=$id";
        $result = $db->con->query($sql);
        $msg = "Updated record for student id $id.";
        header('Location: ./view.php?message=' . urlencode($msg));
    } else {
        if (isset($fields['id']) && count($fields) <= 1)
            $error = 'You must enter a new value in one or more of the fields.';
        else
            $error = 'You must provide the ID of the student whose record is to be updated.';
    }
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
    	<title>Student Records</title>
    	<link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        
        <?php include('inc/userinfo.php'); ?>
        
        <h1>Student Records</h1>
        
        <?php if ($error): ?>
            <div id="status" class="error">
                <?php echo $error; ?>
            </div>
        <?php endif ?>
        
        <div id="container">
            <h2>Update Student Record</h2>
            
            <p>Fill out the form below to update a student record.  You
            <em>must</em> fill in the student ID and any fields you wish
            to update.</p>
            
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                <table cellspacing="0">
                    <tr>
                        <th>ID</th>
                        <th>Last name</th>
                        <th>First name</th>
                        <th>Major</th>
                        <th>GPA</th>
                        <th>&nbsp;</th>
                    </tr>
                    <tr>
                        <td><input name="id" type="text" size="5" value="<?php echo $id; ?>" /></td>
                        <td><input name="last" type="text" /></td>
                        <td><input name="first" type="text" /></td>
                        <td><input name="major" type="text" /></td>
                        <td><input name="gpa" type="text" size="5" /></td>
                        <td><input type="submit" value="Update Record" /></td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>
