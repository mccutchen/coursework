<?php
require_once('inc/utils.php');
require_once('inc/authlib.php');
require_once('inc/db.php');
$auth->authenticate();

$filters = null;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $filters = $db->extractFields($_POST);
}

$sql = 'SELECT * FROM students';
if ($filters)
    $sql .= ' WHERE ' . $db->whereClause($filters);

$result = $db->con->query($sql);

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
    	<title>Student Records</title>
    	<link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        
        <?php include('inc/userinfo.php'); ?>
        
        <h1>Student Records</h1>
        
        <?php if (isset($_GET['message'])): ?>
            <div id="status" class="success">
                <?php echo $_GET['message']; ?>
            </div>
        <?php endif ?>
        
        <?php if ($result->num_rows < 1): ?>
            <div id="status" class="error">
                No student records found matching the filter.
                <a href="<?php echo $_SERVER['PHP_SELF']; ?>">Clear filter?</a>
            </div>
        <?php endif ?>
        
        <div id="container">
            <h2>View Student Records</h2>
            
            <?php if ($result->num_rows > 0): ?>
                <table cellspacing="0">
                    <tr>
                        <th>ID</th>
                        <th>Last name</th>
                        <th>First name</th>
                        <th>Major</th>
                        <th>GPA</th>
                    </tr>
                    <?php while ($row = $result->fetch_row()): ?>
                        <tr>
                            <?php foreach ($row as $index => $field): ?>
                                <td>
                                    <?php
                                        if ($index == 0)
                                            echo sprintf('%04d', $field);
                                        elseif ($index == 4)
                                            echo sprintf('%01.2f', $field);
                                        else
                                            echo $field;
                                    ?>
                                </td>
                            <?php endforeach ?>
                        </tr>
                    <?php endwhile ?>
                </table>
            <?php endif ?>
            
            <h3>Filter Records</h3>
            <p>Only show students whose records match the following fields.
            You may enter a value in one or more of these fields.</p>
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                <table cellspacing="0">
                    <tr>
                        <th>ID</th>
                        <th>Last name</th>
                        <th>First name</th>
                        <th>Major</th>

                        <th>GPA</th>
                        <th>&nbsp;</th>
                    </tr>
                    <tr>
                        <td><input name="id" type="text" size="5" /></td>
                        <td><input name="last" type="text" /></td>
                        <td><input name="first" type="text" /></td>
                        <td><input name="major" type="text" /></td>
                        <td><input name="gpa" type="text" size="5" /></td>
                        <td><input type="submit" value="Filter Records" /></td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>
<?php $result->free(); ?>