<?php

// Show all errors by default
error_reporting(E_ALL);
ini_set('display_errors', 'true');

function getdefault($key, &$array, $default = null) {
    return isset($array[$key]) ? $array[$key] : $default;
}

function getfromrequest($key) {
    return getdefault($key, $_POST, getdefault($key, $_GET));
}

function loaddb($path) {
    $db = array();
    foreach (file($path) as $line) {
        if ($line = trim($line)) {
            list($key, $value) = explode(':', $line, 2);
            $db[$key] = $value;
        }
    }
    return $db;
}
function savedb($db, $path) {
    $s = '';
    foreach ($db as $key => $value) {
        $s .= "$key:$value\n";
    }
    file_put_contents($path, $s);
}

function thispage() {
    return 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}

function filterempties($arr) {
    $result = array();
    foreach ($arr as $key => $val) {
        if (!is_null($val) && $val != '')
            $result[$key] = $val;
    }
    return $result;
}