<?php

class StudentDB {
    private $host = 'z.cs.utexas.edu';
    private $user = 'mccutch';
    private $pass = 'stocarle';
    private $name = 'dbmccutch';
    private $table = 'students';
    
    public $con;
    
    public $fields = array('id', 'last', 'first', 'major', 'gpa');
    
    public function __construct() {
        $this->con = new mysqli(
            $this->host, $this->user, $this->pass, $this->name
        );
        
        if (mysqli_connect_errno())
            die('mysqli_connect failed: ' . mysqli_connect_error());
    }
    
    public function escape($value) {
        if (is_numeric($value))
            return $value;
        else
            return "'" . $this->con->real_escape_string($value) . "'";
    }
    
    public function clauseMaker($fields, $joiner) {
        $clauses = array();
        foreach ($fields as $field => $value) {
            $clauses[] = "$field=" . $this->escape($value);
        }
        return implode($joiner, $clauses);
    }
    public function whereClause($fields) {
        return $this->clauseMaker($fields, ' AND ');
    }
    public function updateClause($fields) {
        return $this->clauseMaker($fields, ', ');
    }
    
    public function extractFields($arr) {
        $fields = array();
        foreach ($this->fields as $field) {
            if (array_key_exists($field, $arr) && $arr[$field] != '') {
                if ($field == 'id')
                    $fields[$field] = intval($arr[$field]);
                elseif ($field == 'gpa')
                    $fields[$field] = floatval($arr[$field]);
                else
                    $fields[$field] = strval($arr[$field]);
            }
        }
        return $fields;
    }
}

// Make an instance that can be used on all pages
$db = new StudentDB();