<?php
    require_once('utils.php');
    require_once('authlib.php');
?>
<div id="user">
    <?php if ($user = $auth->getCurrentUser()): ?>
        Logged in as <strong><?php echo $user; ?></strong>.
        <a href="authenticate.php?action=logout">Log out?</a>
    <?php else: ?>
        Not logged in.
        <a href="authenticate.php?next=<?php echo thispage(); ?>">Log in / sign up?</a>
    <?php endif; ?>
</div>