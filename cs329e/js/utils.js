function $(o) {
    // Return either a single DOM object or an array of DOM objects. If o
    // is a string, it is assumed to be the id of an object.  It o is an
    // HTMLCollection, an array of its elements is returned.  Otherwise,
    // o is assumed to already be a DOM object.
    if (typeof(o) == 'string')
        return document.getElementById(o);
    else if (o.length != undefined) {
        var arr = [];
        $.each(o)(function(el) {
            arr.push(el);
        });
        return arr;
    }
    return o;
}

// Call a function when the DOM is loaded
$.onload = function(callback) {
    document.addEventListener('DOMContentLoaded', callback, false);
};

// Bind a method to a particular scope.  Helps keep 'this' pointing to the
// appropriate place in instance methods used as event handlers.
$.bind = function(method, scope){
    return function(){
        method.apply(scope,arguments);
    }
}

// Simple Ajax requests
$.ajax = function(method, url, data, callback) {
    // Prepare the parameters.
    data = data || {};
    var paramList = [];
    for (key in data)
        paramList.push(key + '=' + (method == 'GET' ? escape(data[key]) : data[key]));
    var params = paramList.join('&');
    
    // Add the params to the URL if this is a GET request
    if (method == 'GET')
        url += '?' + params;
    
    // Create and prepare the request object.
    var xhr = new XMLHttpRequest();
    xhr.open(method, url, true);
    if (method == 'POST') {
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.setRequestHeader("Content-length", params.length);
        xhr.setRequestHeader("Connection", "close");
    }
    
    // Connect up the callback.
    xhr.onreadystatechange = function() {
        // Only call the callback function when the data is ready and
        // the response was successful.
        if (xhr.readyState == 4 && xhr.status == 200)
            callback(xhr);
    }
    
    // Send the request
    xhr.send((method == 'POST') ? params : null);
}

// Functional iterators which will handle either array-like or
// dictionary-like objects.
$.iter = function(obj) {
    return function(fn) {
        if (obj.length != undefined) {
            for (var i = 0; i < obj.length; i++) {
                fn(i, obj[i]);
            }
        } else {
            for (key in obj) {
                fn(key, obj[key]);
            }
        }
    };
};
$.each = function(obj) {
    return function(fn) {
        $.iter(obj)(function(x, o) {
            fn(o);
        });
    };
};


// Adds an 'each' method to the Array object for simpler iteration.
Array.prototype.each = function(fn) {
    for (var i = 0; i < this.length; i++) {
        fn(this[i]);
    }
};

Array.prototype.map = function(fn) {
    var results = [];
    for (var i = 0; i < this.length; i++) {
        results.push(fn(this[i]));
    }
    return results;
};
Array.prototype.filter = function(test) {
    test = test || function(el) { return el ? true : false; };
    var results = [];
    for (var i = 0; i < this.length; i++)
        if (test(this[i]))
            results.push(this[i]);
    return results;
};
Array.prototype.any = function(test) {
    return this.filter(test).length > 0;
};


// Convenient random integer generation
$.rand = function(low,hi) {
    return Math.round(Math.random() * (hi - low)) + low;
}


if (!console) {
    var console = {
        log: function() {}
    };
}