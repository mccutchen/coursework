<?php

function getdefault($key, &$array, $default = null) {
    return isset($array[$key]) ? $array[$key] : $default;
}

function getfromrequest($key) {
    return getdefault($key, $_POST, getdefault($key, $_GET));
}

function loaddb($path) {
    $db = array();
    foreach (file($path) as $line) {
        if ($line = trim($line)) {
            list($key, $value) = explode(':', $line, 2);
            $db[$key] = $value;
        }
    }
    return $db;
}
function savedb($db, $path) {
    $s = '';
    foreach ($db as $key => $value) {
        $s .= "$key:$value\n";
    }
    file_put_contents($path, $s);
}