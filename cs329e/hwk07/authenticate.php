<?php
require('authlib.php');
require_once('utils.php');

$action = getfromrequest('action');
$authtype = getfromrequest('authtype');
$next = getfromrequest('next');
$message = getfromrequest('message');

if ($authtype == 'session')
    $auth = new SessionAuth();
else
    $auth = new CookieAuth();

$success = false;

if ($action == 'login') {
    $success = $auth->logIn($_POST['username'], $_POST['password']);
    if ($success && $next) header("Location: $next");
} elseif ($action == 'signup') {
    $success = $auth->signUp($_POST['username'], $_POST['password']);
    if ($success && $next) header("Location: $next");
} elseif ($action == 'logout') {
    $success = $auth->logOut();
}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
    	<title>Log In / Sign Up</title>
    	<link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body id="reg">
        <h1>Log In / Sign Up</h1>
        
        <?php if ($action && !$success) { ?>
            <div id="status" class="error">
                <?php if ($action == 'login') { ?>
                    Login failed.  Please make sure your username and
                    password are correct.
                <?php } elseif ($action == 'signup') { ?>
                    Signup failed.  Please try another username.
                <?php } else { ?>
                    An unknown error occurred.  Please try again.
                <?php } ?>
            </div>
        <?php } ?>
        
        <?php if ($message): ?>
            <div id="status" class="neutral">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        
        <div id="container">            
            <?php if ($action && $success) { ?>
                <p>
                <?php if ($action == 'login') { ?>
                    <strong>Login successful.</strong>  Thank you for
                    logging in.  You may now go about your business.
                <?php } elseif ($action == 'signup') { ?>
                    <strong>Signup successful.</strong>  Your username is
                    <strong><?php echo $_POST['username']; ?></strong> and
                    your password is <strong><?php echo $_POST['password']; ?></strong>.
                <?php } elseif ($action == 'logout') { ?>
                    <strong>Logged out.</strong>  You have successfully
                    logged out.
                <?php } ?>
                </p>
            <?php } else { ?>
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                    <h2>Log In</h2>
                    <label>Username: <input name="username" type="text" /></label>
                    <label>Password: <input name="password" type="password" /></label>
                    <input type="submit" value="Log In" />
                    <input type="hidden" name="action" value="login" />
                    <input type="hidden" name="authtype" value="<?php echo $authtype; ?>" />
                    <?php if ($next) { ?>
                        <input type="hidden" name="next" value="<?php echo $next; ?>" />
                    <?php } ?>
                </form>
        
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                    <h2>Sign Up</h2>
                    <label>Username: <input name="username" type="text" /></label>
                    <label>Password: <input name="password" type="password" /></label>
                    <input type="submit" value="Sign Up" />
                    <input type="hidden" name="action" value="signup" />
                    <input type="hidden" name="authtype" value="<?php echo $authtype; ?>" />
                    <?php if ($next) { ?>
                        <input type="hidden" name="next" value="<?php echo $next; ?>" />
                    <?php } ?>
                </form>
            <?php } ?>
            
            <p><strong>Note:</strong>  The login database is shared between
            the <a href="newspaper.php">newspaper headlines page</a> and the
            <a href="quiz.php">quiz</a>, so you can use the same login
            information for both.</p>
        </div>
    </body>
</html>
