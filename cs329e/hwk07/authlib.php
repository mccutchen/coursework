<?php

require_once('utils.php');

abstract class Auth {
    protected $path = 'passwd';
    protected $users = array();
    
    public function __construct() {
        $this->users = loaddb($this->path);
    }
    
    private function save() {
        savedb($this->users, $this->path);
    }
    
    public function signUp($user, $password) {
        if (!array_key_exists($user, $this->users)) {
            $this->users[$user] = $password;
            $this->save();
            return $this->logIn($user, $password);
        }
        return false;
    }
    
    public function authenticate($next='', $message='') {
        if ($this->isLoggedIn()) {
            return $this->getCurrentUser();
        } else {
            $host = $_SERVER['HTTP_HOST'];
            $path = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
            $type = preg_replace('/^([A-z]+)auth$/', '$1', strtolower(get_class($this)));
            $url = "http://$host$path/authenticate.php?authtype=$type";
            if ($next) $url .= '&next=' . $next;
            if ($message) $url .= '&message=' . $message;
            header("Location: $url");
        }
    }
    
    public abstract function logIn($user, $password);
    public abstract function logOut();
    public abstract function isLoggedIn();
    public abstract function getCurrentUser();
}

class CookieAuth extends Auth {
    protected $cookieName = 'username';
    protected $cookiePath = '';
    
    public function __construct() {
        parent::__construct();
        $this->cookiePath = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    }

    public function logIn($user, $password) {
        if (!array_key_exists($user, $this->users))
            return false;
        if ($this->users[$user] == $password) {
            $timeout = time() + 3600 * 24 * 30;
            setcookie($this->cookieName, $user, $timeout, $this->cookiePath);
            return true;
        }
        return false;
    }
    public function logOut() {
        $timeout = time() - 3600;
        setcookie($this->cookieName, '', $timeout, $this->cookiePath);
        return true;
    }
    public function isLoggedIn() {
        return isset($_COOKIE[$this->cookieName]);
    }
    public function getCurrentUser() {
        return isset($_COOKIE[$this->cookieName]) ? $_COOKIE[$this->cookieName] : null;
    }
}

class SessionAuth extends Auth {
    public function __construct() {
        parent::__construct();
        session_start();
    }
    public function logIn($user, $password) {
        if (!array_key_exists($user, $this->users))
            return false;
        if ($this->users[$user] == $password) {
            $_SESSION['username'] = $user;
            $_SESSION['login'] = time();
            return true;
        }
        return false;
    }
    public function logOut() {
        $_SESSION = array();
        if (isset($_COOKIE[session_name()]))
            setcookie(session_name(), '', time()-42000, '/');
        session_destroy();
        return true;
    }
    public function isLoggedIn() {
        return isset($_SESSION['username']);
    }
    public function getCurrentUser() {
        return isset($_SESSION['username']) ? $_SESSION['username'] : null;
    }
}