<?php

require('authlib.php');
require_once('utils.php');

$auth = new SessionAuth();
$auth->authenticate(
    'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
    'You must log in or sign up to take the quiz.'
);

$user = $auth->getCurrentUser();

$questions = array(
    'false',
    'true',
    'b',
    'd',
    'galaxy',
    'age'
);

function grade($q) {
    global $questions;
    $i = $q - 1;
    if ($answer = getfromrequest("q$q"))
        return eregi($questions[$i], $answer);
    else
        return false;
}

$message = '';

if (!isset($_SESSION['q']))
    $_SESSION['q'] = 1;

$q = intval(getfromrequest('q'));
if (!$q)
    $q = 0;

$scoresPath = 'results';
$scores = loaddb($scoresPath);

$alreadyTaken = (array_key_exists($user, $scores) && $q == 0);

if (!array_key_exists($user, $scores))
    $scores[$user] = 0;


if ($q > 0 && $q < $_SESSION['q']) {
    $message = "You already answered question $q.  You cannot answer " .
               "the same question more than once.";
}
elseif ($q > 0 && $q < count($questions)) {
    if (grade($q)) {
        $scores[$user] += 10;
        $message = "You got question $q right.";
    } else {
        $message = "You got question $q wrong.";
    }
    $_SESSION['q'] = $q + 1;
    savedb($scores, $scoresPath);
}

$q++;

$timeLimit = 15 * 60;
$timeTaken = time() - $_SESSION['login'];
$timeOut = ($timeTaken > $timeLimit);
$finished = ($q > count($questions) || $timeOut || $alreadyTaken);

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
    	<title>Astronomy Quiz</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body id="quiz">
        
        <div id="user">
            <?php if ($user): ?>
                Logged in as <strong><?php echo $user; ?></strong>.
                <a href="authenticate.php?authtype=session&amp;action=logout">Log out?</a>
            <?php else: ?>
                Not logged in.
                <a href="authenticate.php?authtype=session">Log in / sign up?</a>
            <?php endif; ?>
        </div>
        
        <h1>Astronomy Quiz</h1>
        
        <?php if ($message && !$finished): ?>
            <div id="status">
                <?php if ($message) echo $message; ?>
                <?php if (!$finished): ?>
                    <strong>Current score: <?php echo $scores[$user]; ?></strong>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        
        <div id="container">
            <?php if (!$finished): ?>
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                <ol start="<?php echo $q; ?>">
                    <li>
                    <?php if ($q == 1): ?>
                        <p>According to Kepler the orbit of the earth is a
                        circle with the sun at the center.</p>
                        <label>
                            <input name="q1" type="radio" value="true" />
                            True
                        </label><br />
                        <label>
                            <input name="q1" type="radio" value="false" />
                            False
                        </label>
                    <?php elseif ($q == 2): ?>
                        <p>Ancient astronomers did consider the heliocentric
                        model of the solar system but rejected it because they
                        could not detect parallax.</p>
                        <label>
                            <input name="q2" type="radio" value="true" />
                            True
                        </label><br />
                        <label>
                            <input name="q2" type="radio" value="false" />
                            False
                        </label>
                    <?php elseif ($q == 3): ?>
                        <p>The total amount of energy that a star emits is
                        directly related to its</p>
                        <label>
                            <input name="q3" type="radio" value="a" />
                            surface gravity and magnetic field
                        </label><br />
                        <label>
                            <input name="q3" type="radio" value="b" />
                            radius and temperature
                        </label><br />
                        <label>
                            <input name="q3" type="radio" value="c" />
                            pressure and volume
                        </label><br />
                        <label>
                            <input name="q3" type="radio" value="d" />
                            location and velocity
                        </label>
                    <?php elseif ($q == 4): ?>
                        <p>Stars that live the longest have</p>
                        <label>
                            <input name="q4" type="radio" value="a" />
                            high mass
                        </label><br />
                        <label>
                            <input name="q4" type="radio" value="b" />
                            high temperature
                        </label><br />
                        <label>
                            <input name="q4" type="radio" value="c" />
                            lots of hydrogen
                        </label><br />
                        <label>
                            <input name="q4" type="radio" value="d" />
                            low mass
                        </label><br />
                    <?php elseif ($q == 5): ?>
                        <p>A collection of gas, dust, and a hundred billion
                        stars is called a(n) <input name="q5" type="text"
                        size="8" />.</p>
                    <?php elseif ($q == 6): ?>
                        <p>The inverse of Hubble's constant is a measure of
                        the <input name="q6" type="text" size="5" /> of the
                        universe.</p>
                    </li>
                    <?php endif; ?>
                    
                    <input type="submit" value="That's my final answer." />
                    <input type="hidden" name="q" value="<?php echo $q; ?>" />
                </ol>
            </form>
            <?php else: ?>
                <?php if ($timeOut): ?>
                    <p>Sorry, you've taken more than 15 minutes to take
                    the quiz.</p>
                <?php elseif ($alreadyTaken): ?>
                    <p>Sorry, but it appears that you've already taken
                    the quiz before.  You're only allowed to take it
                    once.</p>
                <?php else: ?>
                    <p>Congratulations, you've finished the quiz!</p>
                <?php endif; ?>
                <h3>Your final score: <?php echo $scores[$user]; ?> /
                    <?php echo 10 * count($questions); ?></h3>
                
                <?php if (count($scores) > 1): ?>
                    <p>Everyone's scores:</p>
                    <table cellspacing="0">
                        <tr>
                            <th>Username</th>
                            <th>Score</th>
                        </tr>
                        <?php foreach ($scores as $u => $s): ?>
                            <tr>
                                <td><?php echo $u; ?></td>
                                <td><?php echo $s; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </body>
</html>
