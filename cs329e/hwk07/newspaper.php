<?php

require('authlib.php');
$auth = new CookieAuth();

$stories = array(
    'Election Unleashes a Flood of Hope Worldwide',
    'Russia Warns of Missile Deployment',
    'Plane Crash Kills Mexico&#8217;s Point Man in Drug War',
    'China Has Sentenced 55 Over Tibet Riot in March',
    'Minister&#8217;s Dismissal Is Setback for Iranian Leader'
);

$story = null;

if (isset($_GET['storyId'])) {
    $thisPage = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $auth->authenticate($thisPage);
    $id = intval($_GET['storyId']);
    $story = $stories[$id];
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
    	<title>Headlines</title>
    	<link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body id="newspaper">
        
        <div id="user">
            <?php if ($user = $auth->getCurrentUser()): ?>
                Logged in as <strong><?php echo $user; ?></strong>.
                <a href="authenticate.php?authtype=cookie&amp;action=logout">Log out?</a>
            <?php else: ?>
                Not logged in.
                <a href="authenticate.php?authtype=cookie">Log in / sign up?</a>
            <?php endif; ?>
        </div>
        
        <h1><?php echo ($story) ? $story : 'Headlines'; ?></h1>
        
        <div id="container">
            <?php if (!$story): ?>
                <ul>
                    <?php
                        foreach ($stories as $id => $story) {
                            $url = "{$_SERVER['PHP_SELF']}?storyId=$id";
                            echo "<li><h2><a href=\"$url\">$story</a></h2></li>\n";
                        }
                    ?>
                </ul>
            <?php else: ?>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam eleifend. Integer sit amet leo sed erat lacinia tempor. Mauris hendrerit tellus ut leo. Cras bibendum eleifend sapien. Morbi non sapien. Aenean eget ante. Sed quis eros. Praesent tincidunt, ligula in auctor sollicitudin, dui velit euismod nisi, id auctor erat diam et magna. Vestibulum ultrices sem id leo. Phasellus sit amet enim tincidunt tellus fermentum convallis. Vestibulum mollis, eros vitae egestas fringilla, nisi dolor cursus tortor, sit amet eleifend dolor dui at nisl.</p>
                <p>Fusce mattis. Etiam et enim. Pellentesque pede velit, pharetra iaculis, elementum ut, condimentum et, tortor. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur ullamcorper, magna quis pellentesque aliquam, tellus dui hendrerit odio, sed laoreet metus velit eget metus. Proin tellus est, commodo sit amet, commodo in, mollis non, dui. Phasellus eu lacus. Quisque id erat. Praesent blandit ipsum quis elit. Vivamus ac turpis.</p>
            <?php endif; ?>
        </div>
    </body>
</html>
