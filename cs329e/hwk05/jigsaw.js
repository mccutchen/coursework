var dragobj = null;
var zindex = 1;
var startedAt = null;

function mousemove(event) {
    if (window.dragobj) {
        var maybeSlot = Puzzle.shouldSnapTo(window.dragobj);
        if (maybeSlot && !Puzzle.shouldUnsnap(event))
            maybeSlot.setPiece(window.dragobj);
        else {
            if (maybeSlot)
                maybeSlot.removePiece();
            window.dragobj.moveTo(
                event.clientX - window.dragobj.mouseOffset.x,
                event.clientY - window.dragobj.mouseOffset.y
            );
        }
    }
    event.preventDefault();
    return false;
}

function mouseup(event) {
    window.dragobj = null;
}

function done(event) {
    var secondsTaken = Math.round((new Date() - window.startedAt) / 1000);
    var message;
    if (Puzzle.test()) {
        message = 'Congratulations, you solved the puzzle!';
    } else {
        message = 'Better luck next time.';
    }
    $('status').innerHTML = message + ' You took ' + formatSeconds(secondsTaken) + '.';
}

function formatSeconds(time) {
    var seconds = time % 60;
    var minutes = (time / 60) % 60;
    var hours = (time / 3600) % 24;
    var days = Math.floor((time / 3600) / 24);
    hours += days * 24;
    
    function format(n, type) {
        return n + ' ' + type + ((n > 1) ? 's' : '');
    }
    var s = format(seconds, 'second');
    if (minutes > 1) s = format(minutes, 'minute') + s;
    if (hours > 1) s = format(hours, 'hour') + s;
    return s;
}


$.onload(function() {    
    Puzzle.init('puzzle');
    document.addEventListener('mousemove', mousemove, true);
    document.addEventListener('mouseup', mouseup, true);
    $('done').addEventListener('click', done, false);
    window.startedAt = new Date();
});
