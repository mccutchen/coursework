function Slot(id, x, y) {
    this.id = id;
    this.x = x;
    this.y = y;
    this.piece = null;

    this.test = function() {
        return (this.piece && this.piece.id == this.id);
    };

    this.isClose = function(piece) {
        return (Math.abs(this.x - piece.x) <= Puzzle.snapThreshold &&
                Math.abs(this.y - piece.y) <= Puzzle.snapThreshold);
    };
    
    this.setPiece = function(piece) {
        if (!this.hasPiece()) {
            piece.moveTo(this.x, this.y);
            this.piece = piece;
        }
    };
    this.hasPiece = function(piece) {
        if (piece) 
            return this.piece == piece;
        else
            return (this.piece ? true : false);
    };
    this.removePiece = function() {
        this.piece = null;
    };
}
