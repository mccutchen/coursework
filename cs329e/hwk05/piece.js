function Piece(set, number) {
    var el = (function() {
        var piece = document.createElement('img');
        piece.src = 'img/img' + set + '-' + number + '.jpg';
        piece.setAttribute('class', 'piece');
        document.body.appendChild(piece);
        return piece;
    })();
    
    this.id = number;
    this.x = parseInt(el.style.left ? el.style.left : el.offsetLeft, 10);
    this.y = parseInt(el.style.top ? el.style.top : el.offsetTop, 10);
    this.mouseOffset = { x: 0, y: 0 };

    this.moveTo = function(x, y) {
        this.x = x;
        this.y = y;
        el.style.left = x + 'px';
        el.style.top = y + 'px';
    };
    
    this.setMouseOffset = function(event) {
        this.mouseOffset.x = event.clientX - this.x;
        this.mouseOffset.y = event.clientY - this.y;
    };
    
    this.mouseDown = function(event) {
        window.dragobj = this;
        el.style.zIndex = ++zindex;
        this.setMouseOffset(event);
        event.preventDefault();
        return false;
    }
    
    el.addEventListener('mousedown', $.bind(this.mouseDown, this), false);
}

Piece.size = 100;