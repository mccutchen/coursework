var Puzzle = {
    slots: [],
    pieces: [],
    origin: {
        x: 0,
        y: 0
    },
    
    cols: 4,
    rows: 3,
    size: null,
    
    snapThreshold: 15,
    unsnapThreshold: 25,
    
    numSets: 3,

    init: function(element) {
        element = $(element);
        this.origin.x = element.offsetLeft;
        this.origin.y = element.offsetTop;
        this.size = this.cols * this.rows;
        
        // Which puzzle pieces are we using?
        var set = $.rand(1, this.numSets);
        
        for (var row = 1; row <= this.rows; row++) {
            for (var col = 1; col <= this.cols; col++) {
                var slotx = (col - 1) * Piece.size + this.origin.x;
                var sloty = (row - 1) * Piece.size + this.origin.y;
                var id = String(col + this.cols * (row - 1));
                
                this.slots.push(new Slot(id, slotx, sloty));
                this.pieces.push(new Piece(set, id));
            }
        }
        
        this.scatterPieces();
    },
    
    test: function() {
        var correct = Puzzle.slots.filter(function(slot) {
            return slot.test();
        });
        return correct.length == this.slots.length;
    },
    
    shouldSnapTo: function(piece) {
        var snapSlots = Puzzle.slots.filter(function(slot) {
            return slot.isClose(piece) &&
                   (slot.hasPiece(piece) || !slot.hasPiece());
        });
        if (snapSlots.length > 0)
            return snapSlots[0];
        else
            return null;
    },
    
    shouldUnsnap: function(event) {
        if (window.dragobj) {
            var x = event.clientX - window.dragobj.mouseOffset.x;
            var y = event.clientY - window.dragobj.mouseOffset.y;
            return (Math.abs(x - window.dragobj.x) >= this.unsnapThreshold ||
                    Math.abs(y - window.dragobj.y) >= this.unsnapThreshold);
        }
        return false;
    },
    
    scatterPieces: function() {
        var puzzle = $('board');
        var minLeft = puzzle.offsetLeft + puzzle.clientWidth + 10;
        var minTop = puzzle.offsetTop;
        var maxLeft = document.documentElement.clientWidth - Piece.size;
        var maxTop = puzzle.offsetTop + puzzle.clientHeight - Piece.size;
        this.pieces.each(function(piece) {
            piece.moveTo($.rand(minLeft, maxLeft), $.rand(minTop, maxTop));
        });
    }
};
