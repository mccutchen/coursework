/*
 * File: BankAccount.java
 * 
 * Description: Model a bank account, which has an account number and a balance,
 * along with methods to check the balance, get the account number, and deposit
 * and withdraw funds.
 * 
 * Name: Will McCutchen
 * 
 * UT EID: willm
 * 
 * Course Name: CS 313E
 * 
 * Unique Number: 56440
 * 
 * Date Created: 09/15/2007
 * 
 * Date Last Modified: 09/16/2007
 */


public class BankAccount {
    
    // Keep track of account numbers
    private static int ACCOUNT_COUNTER = 1;
    
    // How much to charge the account when it is overdrawn
    private final static double OVERDRAFT_FEE = 25.0;
    
    // Default balance to use when creating a new account
    private final static double DEFAULT_BALANCE = 0.0;
    
    // Instance variables for storing the account balance and number
    private double balance;
    private int accountNumber;
    
    
    // Constructors
    public BankAccount(double balance) {
        // Only use the given balance if it is non-negative
        if (balance >= 0) {
            this.balance = balance;
        } else {
            this.balance = DEFAULT_BALANCE;
        }
        this.accountNumber = ACCOUNT_COUNTER++;
    }
    public BankAccount() {
        this(DEFAULT_BALANCE);
    }
    
    
    // Getters
    public int getAccountNumber() {
        return accountNumber;
    }
    public double getBalance() {
        return balance;
    }
    
    
    // Only deposit the given amount if it is non-negative
    public void deposit(double amount) {
        if (amount >= 0.0) {
            balance += amount;
        }
    }
    
    // If the amount to withdraw is greater than the available balance, charge
    // an overdraft fee and don't make the withdrawal.
    public void withdraw(double amount) {
        // Don't withdraw a negative amount
        if (amount > 0) { 
            // If we have sufficient funds, make the withdrawal
            if (amount <= balance) { balance -= amount; }
            
            // Otherwise, charge an overdraft fee
            else { balance -= OVERDRAFT_FEE; }
        }
    }
}
