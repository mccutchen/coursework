/*
 * File: AccountTest.java
 * 
 * Description: Tests for the BankAccount object.
 * 
 * Name: Will McCutchen
 * 
 * UT EID: willm
 * 
 * Course Name: CS 313E
 * 
 * Unique Number: 56440
 * 
 * Date Created: 09/15/2007
 * 
 * Date Last Modified: 09/16/2007
 */

public class AccountTest {
    
    public static void main(String[] args) {
        System.out.println("Testing BankAccount");
        System.out.println("-------------------");
        
        // Create some accounts to test, one with a given starting balance and
        // one with the default balance.
        BankAccount account1 = new BankAccount(100.0);
        BankAccount account2 = new BankAccount();
        BankAccount account3 = new BankAccount();

        // ====================================================================
        // Test account numbers
        // ====================================================================
        testValues("Testing first account number", account1.getAccountNumber(), 1);
        testValues("Testing second account number", account2.getAccountNumber(), 2);
      
        // ====================================================================
        // Test initial balances
        // ====================================================================
        testValues("Testing balance given to constructor", account1.getBalance(), 100.0);
        testValues("Testing default balance", account2.getBalance(), 0.0);
        testValues("Testing negative balance given to constructor", account3.getBalance(), 0.0);

        // ====================================================================
        // Test withdrawals
        // ====================================================================
        account1.withdraw(10.0);
        testValues("Testing normal withdrawal", account1.getBalance(), 90.0);
        
        account2.withdraw(10.0);
        testValues("Testing an overdraft", account2.getBalance(), -25.0);
        
        // Withdrawing a negative amount should fail
        account1.withdraw(-100.0);
        testValues("Testing a negative withdrawal", account1.getBalance(), 90.0);
        
        // ====================================================================
        // Test deposits
        // ====================================================================
        account1.deposit(100.0);
        testValues("Testing normal deposit", account1.getBalance(), 190.0);
        
        account2.deposit(100.0);
        testValues("Testing normal deposit", account2.getBalance(), 75.0);
        
        // Try depositing a negative amount, which should fail
        account2.deposit(-100);
        testValues("Testing negative deposit", account2.getBalance(), 75.0);
        
        // Finished!
        System.out.println("---------");
        System.out.println("Finished.");
    }
    
    
    /*
     * Helper method for testing the equality of two values and printing the
     * results. If the two values are not equal, the expected and actual results
     * are printed.
     */
    private static void testValues(String description, double actualResult, double expectedResult) {
        System.out.print(description + "... ");
        if (expectedResult != actualResult) {
            System.out.println("FAILED! (expected: " + expectedResult + "; got: " + actualResult + ")");
        } else {
            System.out.println("Passed.");
        }
    }
}
