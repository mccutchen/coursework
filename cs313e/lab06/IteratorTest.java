/*
 * File: IteratorTest.java
 * 
 * Description: Create and test a class that implements the
 * java.util.Iterator interface and iterates over the integers 1
 * through 10 as strings.
 *
 * Name: Will McCutchen
 * 
 * UT EID: willm
 * 
 * Course Name: CS 313E
 * 
 * Unique Number: 56440
 * 
 * Date Created: 11/17/2007
 * 
 * Date Last Modified: 11/17/2007
 */

import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * IteratorTest tests the SimpleIterator class by creating a
 * SimpleIterator and printing each item over which it iterates.
 */
public class IteratorTest {
    
    public static void main(String[] args) {
        System.out.println("Testing my custom iterator class...");
        
        // Instantiate our iterator
        SimpleIterator it = new SimpleIterator();

        // Print every item in the iterator
        while (it.hasNext()) {
            System.out.println(it.next());
        }

        // Try to remove the last item.  This should print a message
        // indicating that removing elements is not allowed.
        it.remove();
    }
}


/*
 * SimpleIterator implements the Iterator interface and simply
 * iterates over the integers 1 through 10 as strings.
 */
class SimpleIterator implements Iterator {
    
    private String[] contents;
    private int cursor;

    public SimpleIterator() {
        // Initialize the array of strings over which we will iterate
        contents = new String[10];
        for (int i = 0; i < contents.length; i++)
            contents[i] = "" + (i+1);

        // Initialize the cursor, which will keep track of our
        // position in the array as we iterate
        cursor = 0;
    }

    public final boolean hasNext() {
        return cursor < contents.length;
    }

    public final Object next() {
        if (!hasNext())
            throw new NoSuchElementException();
        return contents[cursor++];
    }

    public final void remove() {
        System.out.println("Remove is not allowed");
    }
}
