/*
 * File: StackTest.java
 * 
 * Description: Creates a stack of Strings to test the MyStack
 * implementation.
 *
 * Name: Will McCutchen
 * 
 * UT EID: willm
 * 
 * Course Name: CS 313E
 * 
 * Unique Number: 56440
 * 
 * Date Created: 11/17/2007
 * 
 * Date Last Modified: 11/17/2007
 */

public class StackTest {

    public static void main(String[] args) {
        // Test the MyStack implementation by creating and exercising
        // a stack of Strings.
        
        int stackSize = 3;
        MyStack stack = new MyStack(stackSize);
        
        testSuite("Created new stack with capacity " + stackSize + ".");
        testCase("New stack is empty?",
                 stack.isEmpty());
        testCase("Size of empty stack is 0?",
                 stack.getSize() == 0);
        testCase("Peeking at empty stack returns null?",
                 stack.peek() == null);
        testCase("Popping empty stack returns null?",
                 stack.pop() == null);
        

        // Used in the next few tests
        String testString = "test";
        
        testSuite("Adding string \"" + testString + "\" to stack.");
        testCase("Successfully added string?",
                 stack.push(testString));
        testCase("Stack is no longer empty?",
                 !stack.isEmpty());
        testCase("Size of stack is now 1?",
                 stack.getSize() == 1);
        testCase("Peeking at stack returns \"" + testString + "\"?",
                 stack.peek().equals(testString));

        
        testSuite("Popping one element stack.");
        testCase("Popping stack returns \"" + testString + "\"?",
                 stack.pop().equals(testString));
        testCase("Stack is now empty again?",
                 stack.isEmpty());
        

        testSuite("Filling stack with strings.");
        stack.push("first");
        stack.push("second");
        stack.push("third");

        testCase("Stack no longer empty?",
                 !stack.isEmpty());
        testCase("Size of stack is " + stackSize + "?",
                 stack.getSize() == stackSize);
        testCase("Peeking at stack returns last item added?",
                 stack.peek().equals("third"));
        

        testSuite("Adding an item to a full stack.");
        testCase("Adding item to full stack fails?",
                 stack.push("blah") == false);
        testCase("Size of stack is still " + stackSize + "?",
                 stack.getSize() == stackSize);
        

        testSuite("Peeking and popping.");
        testCase("Popping stack returns last item added?",
                 stack.pop().equals("third"));
        testCase("Size of stack is now " + (stackSize-1) + "?",
                 stack.getSize() == stackSize-1);
        testCase("Peeking at stack returns second to last item added?",
                 stack.peek().equals("second"));


        testSuite("Printing the stack to test toString()");
        stack.push("third"); // refill the stack
        System.out.println(stack);
    }
    
    static void testSuite(String title) {
        System.out.println("\n" + title);
        for(int i = 0; i < title.length(); i++)
            System.out.print("-");
        System.out.print("\n");
    }
    
    static void testCase(String description, boolean result) {
        System.out.println(description + " " + result);
    }
}
