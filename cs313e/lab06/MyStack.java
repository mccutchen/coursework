/*
 * File: MyStack.java
 * 
 * Description: A simple stack implementation, backed by an array of
 * Objects, which provides the methods isEmpty(), getSize(), peek(),
 * pop(), and push().
 *
 * Name: Will McCutchen
 * 
 * UT EID: willm
 * 
 * Course Name: CS 313E
 * 
 * Unique Number: 56440
 * 
 * Date Created: 11/17/2007
 * 
 * Date Last Modified: 11/17/2007
 */

public class MyStack {

    // The elements in this stack.
    private Object[] elements;

    // The index of the topmost element.  Will be -1 if the stack is
    // empty.
    private int head;

    public MyStack(int capacity) {
        elements = new Object[capacity];
        head = -1;
    }

    public boolean isEmpty() {
        return head == -1;
    }
    
    public boolean isFull() {
        return head == elements.length - 1;
    }

    public int getSize() {
        return head + 1;
    }

    public Object peek() {
        if (isEmpty())
            return null;
        return elements[head];
    }

    public Object pop() {
        if (isEmpty())
            return null;

        Object obj = elements[head];
        elements[head--] = null;
        return obj;
    }

    public boolean push(Object obj) {
        if (isFull())
            return false;
        
        elements[++head] = obj;
        return true;
    }

    public String toString() {
        if (isEmpty())
            return "<empty stack>";
        
        String s = "";
        for (int i = head; i >= 0; i--) {
            s += elements[i].toString() + (i > 0 ? "\n":"");
        }
        return s;
    }
}
