/*
 * File: AverageTemp.java
 * 
 * Description: A simple program which prompts the user for air temperatures
 * and reports the high, low and average temperatures entered.
 * 
 * Name: Will McCutchen
 * 
 * UT EID: willm
 * 
 * Course Name: CS 313E
 * 
 * Unique Number: 56440
 * 
 * Date Created: 09/03/2007
 * 
 * Date Last Modified: 09/06/2007
 */

import java.util.*;

/*
 * Prompts the user to input air temperatures as positive integers. When the
 * user inputs a negative temperature, reports the high, low and average
 * temperatures given and exits.
 */
public class AverageTemp {

    public static void main(String[] args) throws java.io.IOException {
        // Variables used for reporting the results
        int high, low, average;

        // A collection to store the temperatures given by the user.
        Vector<Integer> temperatures = new Vector<Integer>();
        
        // An input scanner to get integers from the user
        Scanner inputScanner = new Scanner(System.in);

        // Collect the user's input until a negative temperature is given.
        System.out.println("Enter air temperatures or "
                           + "a number less than 0 to quit:");
        int input = inputScanner.nextInt();
        while (input >= 0) {
            temperatures.add(input);
            input = inputScanner.nextInt();
        }

        // If no temperatures were given, just report that fact and exit.
        if (temperatures.size() < 1) {
            System.out.println("No temperatures given.");
            System.exit(0);
        }

        // We have at least one temperature to work with. First, determine the
        // high and low temperatures given by the user.
        Collections.sort(temperatures);
        low = temperatures.firstElement();
        high = temperatures.lastElement();

        // Next, average the temperatures entered by the user
        float sum = 0;
        for (int temperature : temperatures) {
            sum += temperature;
        }
        average = Math.round(sum / temperatures.size());

        // Finally, report the results
        System.out.println("High Temp: " + high);
        System.out.println("Low Temp:  " + low);
        System.out.println("Average:   " + average);
    }
}