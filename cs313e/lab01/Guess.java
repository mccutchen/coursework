/*
 * File: Guess.java
 * 
 * Description: A guessing game which picks a random number between 1 and 100
 * (inclusive). Exits when the user guesses correctly or enters a negative
 * number.
 * 
 * Name: Will McCutchen
 * 
 * UT EID: willm
 * 
 * Course Name: CS 313E
 * 
 * Unique Number: 56440
 * 
 * Date Created: 09/04/2007
 * 
 * Date Last Modified: 09/06/2007
 */

import java.util.*;

/*
 * Public class whose main method is called when this program is run. Simply
 * instantiates and plays a GuessingGame
 */
public class Guess {
    public static void main(String[] args) {
        GuessingGame game = new GuessingGame();
        game.play();
    }
}


/*
 * Implements the actual guessing game itself: Picks a random number, prompts
 * the user to guess until they get the correct answer or enter a negative
 * number, reports the number of guesses it took.
 */
class GuessingGame {
    // Bookkeeping variables to keep track of the correct answer, the current
    // guess and the number of guesses made so far.
    private int correctAnswer;
    private int guessCount;
    private int currentGuess;

    // The high and low bounds of the random number to generate
    private final int LOW_NUM = 1;
    private final int HIGH_NUM = 100;

    // An input scanner to get integers from the user
    private Scanner inputScanner;

    
    public GuessingGame() {
        // Pick a random number, between LOW_NUM and HIGH_NUM, for the user to
        // guess
        correctAnswer = new Random().nextInt(HIGH_NUM - LOW_NUM) + LOW_NUM;

        // initialize guessCount at 0
        guessCount = 0;

        // Create the scanner used to get user input
        inputScanner = new Scanner(System.in);
    }

    
    /*
     * Implements the actual logic of the game. Tells the user the rules and
     * prompts the user to guess until the user enters the correct answer or a
     * negative number.
     */
    public void play() {
        // Tell the user the rules
        System.out.println("Play the guessing game... guess my random number between " + LOW_NUM + " and " + HIGH_NUM + ".");
        System.out.println("Enter a negative number to quit.\n");

        // Get an initial guess
        currentGuess = getGuess();
        
        // Loop until the users guesses correctly
        while (currentGuess != correctAnswer) {
            // If the user enters a negative number, quit the program
            if (currentGuess < 0) {
                quit();
            }

            // Otherwise, let the user know if their guess is too high or low
            if (currentGuess > correctAnswer) {
                System.out.println(currentGuess + " is too high.");
            } else {
                System.out.println(currentGuess + " is too low.");
            }

            // Then, get a new guess and start over
            currentGuess = getGuess();
        }

        // The user has guessed correctly
        System.out.println(currentGuess + " is right.");
        System.out.println("You needed " + guessCount + " guess" + ((guessCount > 1) ? "es." : "."));
    }

    
    /*
     * Quits the guessing game when the user enters a negative number.
     */
    private void quit() {
        System.out.println("You entered a negative number.  Goodbye!");
        System.exit(0);
    }

    
    /*
     * Gets a guess from the user and returns it. Also increments the guess
     * counter.
     */
    private int getGuess() {
        guessCount++;
        System.out.print("Pick a number between 1 and 100: ");
        return inputScanner.nextInt();
    }
}
