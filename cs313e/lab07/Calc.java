/*
 * File: Calc.java
 * 
 * Description: Create a simple calculator to do addition, subtraction
 * and multiplication of integers.  When the user presses more than
 * one operator key (+, -, or x) more than once, the last operator
 * pressed is used for the calculation.
 *
 * Name: Will McCutchen
 * 
 * UT EID: willm
 * 
 * Course Name: CS 313E
 * 
 * Unique Number: 56440
 * 
 * Date Created: 11/29/2007
 * 
 * Date Last Modified: 12/07/2007
 */
 
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;

public class Calc extends JFrame {

    // Constants to help lay out the GUI
    private static final int DISPLAY_FONT_SIZE = 24;
    private static final int COMPONENT_SPACING = 5;

    // The display, which will hold the current number being entered
    // by the user and display the results of each calculation.
    private JLabel display;

    // One event listener is used by each button
    private CalcListener listener = new CalcListener();

    // A nice border that can be shared by components.
    private final EmptyBorder padding =
        new EmptyBorder(COMPONENT_SPACING,
                        COMPONENT_SPACING,
                        COMPONENT_SPACING,
                        COMPONENT_SPACING);

    // Each component will use the below font or some variation on it.
    private final Font font = new Font("Monospaced", Font.BOLD, 14);


    /**
     * Calc constructor
     *
     * Creates the actual GUI and wires up events, including the
     * window closing event.
     **/
    public Calc() {
        super("Calculator");

        // Create the GUI
        createComponents();

        // Make the program exit when the window/frame is closed.
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    
    /**
     * Main method
     *
     * Creates and displays a Calc object (which inherits from
     * JFrame).  The Calc constructor takes care of creating the GUI
     * and wiring up events.
     **/
    public static void main(String[] args) {
        Calc calc = new Calc();
        calc.pack();
        calc.setResizable(false);
        calc.setVisible(true);
    }


    // Creates each component of the GUI and adds it to this Calc
    // instance.  Calls out to other methods to create the individual
    // components.
    private void createComponents() {
        // Create each individual GUI component.
        Component display = createDisplay();
        Component numbers = createNumbers();
        Component operators = createOperators();

        // Add each component to this instance's content pane.
        getContentPane().add(display, "North");
        getContentPane().add(numbers, "Center");
        getContentPane().add(operators, "East");
    }

    // Creates the display, which shows the user the numbers they
    // enter and the results of their calculations.
    private Component createDisplay() {
        JPanel container = new JPanel(new BorderLayout());
        container.setBackground(Color.WHITE);
        container.setBorder(padding);

        // Initially, the display will just read "0".
        display = new JLabel("0", JLabel.RIGHT);

        // Give the display a larger version of the font used by the
        // rest of the UI.
        display.setFont(font.deriveFont(24f));

        // Add the display to its container and return the container.
        container.add(display, "Center");
        return container;
    }

    // Creates the grid of number keys and wires up their event
    // listeners.
    private Component createNumbers() {
        JPanel container = new JPanel();
        container.setLayout(new GridLayout(4, 3));
        container.setBorder(padding);

        // This loop is a little bit complicated in order to lay the
        // numbers out in the correct order (as far as calculators
        // go).
        for(int i = 9; i >= 0; i -= 3) {
            for (int j = 2; j >= 0; j--) {
                
                // The number for the current button
                int n = i - j;

                // We only add this button if n is non-negative
                if (n >= 0) {
                    JButton button = new JButton("" + (i-j));
                    button.setFont(font);
                    button.addActionListener(listener);

                    // If we're on the 0 button, we need to add an
                    // empty JLabel to take up the first cell on the
                    // last row.  This is small hack.
                    if (n == 0) 
                        container.add(new JLabel(""));

                    // Add our button to its container.
                    container.add(button);
                }
            }
        }
        return container;
    }

    // Creates the operator buttons and wires up their events.  Uses
    // the Unicode MULTIPLICATION SIGN rather than a plain "x".
    private Component createOperators() {
        String[] operators = {
            "+",
            "-",
            "\u00D7", // Unicode MULTIPLICATION SIGN
            "="
        };
        
        JPanel container = new JPanel();
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.setBorder(padding);

        // Loop through each operator, adding it to the container and
        // attaching events.
        for(String op: operators) {
            JButton button = new JButton(op);
            button.setFont(font);
            button.addActionListener(listener);
            container.add(button);
        }
        return container;
    }


    /**
     * Event listener
     *
     * This ActionListener subclass handles all of the events emitted
     * by the calculator buttons.  One instance of this class is
     * shared by each button, which allows state to be maintained in
     * this class.
    **/
    private class CalcListener implements ActionListener {

        // A Calculation object that will be used to model each
        // calculation made.
        Calculation calculation = new Calculation();

        // A flag that will determine when the display is cleared.
        private boolean shouldClearDisplay = false;

        // A flag to help us determine if the user just clicked two
        // operator buttons in a row.
        private boolean justHandledOperator = false;

        // The actual event listener, which simply determines the
        // event's source and delegates to specialized handling
        // methods for numbers and operators.
        public void actionPerformed(ActionEvent e) {
            String buttonText = ((JButton)e.getSource()).getText();

            // Dispatch based on what kind of button was pushed
            if ("0123456789".indexOf(buttonText) != -1)
                handleNumber(Integer.parseInt(buttonText));
            else
                handleOperator(buttonText.charAt(0));
        }

        // Handler for number button pushes.
        private void handleNumber(int numberClicked) {
            
            // If the current number is 0, just replace it with
            // the number that was clicked.
            if (display.getText().equals("0")) {
                display.setText("" + numberClicked);

                // If we needed to clear the display, we no longer
                // need to do it explicitly, since we just overwrote
                // the 0 that was already there.
                if (shouldClearDisplay)
                    shouldClearDisplay = false;
            }

            // If we should clear the display (which is the case after
            // an operator button has been pushed), we replace
            // whatever is there with the current number and reset the
            // flag.
            else if (shouldClearDisplay) {
                display.setText("" + numberClicked);
                shouldClearDisplay = false;
            }

            // Otherwise, just add the number that was just clicked to
            // the end of the number(s) already on the display.
            else
                display.setText(display.getText() + numberClicked);
            
            // We just handled a number, so set the flag to false.
            justHandledOperator = false;
        }

        // Handler for operator button pushes.  If the user presses more
        // than one operator button in succession, the last operator
        // pressed is used in the calculation.
        private void handleOperator(char op) {
            
            // If we just handled an operator, all we do is replace
            // the last operator with the new one.
            if (justHandledOperator) {
                calculation.setOperator(op);
            }

            // Otherwise, we add the current number to the
            // calculation, and maybe the new operator as well.
            else if (!calculation.ready()) {

                calculation.pushValue(getCurrentNumber());

                // We only add the new operator if the calculation
                // does not already have one.  If the calculation
                // already has an operator, that means the user just
                // added the second number to the calculation.
                if (!calculation.hasOperator()) {
                    calculation.setOperator(op);
                }
            }

            // If the calculation is ready, solve it and display the
            // results.
            if (calculation.ready() || op == '=') {
                int result = calculation.solve();
                display.setText("" + result);
            }

            // Once an operator button has been pushed, the next time
            // the user pushes a number button, the display should be
            // cleared.
            shouldClearDisplay = true;

            // We only set this flag if the user pressed an operator
            // other than equals.
            if (op != '=')
                justHandledOperator = true;
        }

        // Parses the current number on the display as an int.
        private int getCurrentNumber() {
            return Integer.parseInt(display.getText());
        }
    }


    // Class that models a simple arithmetic calculation.
    private class Calculation {

        // I couldn't think of the correct name for the values on each
        // side of the operator, so I went with these.
        private int leftSide;
        private int rightSide;
        private char operator;

        private final int INVALID_VALUE = -1;
        private final char INVALID_OP = '~';

        // The list of valid operators.  \u00D7 is Unicode
        // MULTIPLICATION SIGN.
        private final String OPERATORS = "+-\u00D7";

        public Calculation() {
            reset();
        }

        // Reset/initialize the values of this calculation.
        public void reset() {
            leftSide = rightSide = INVALID_VALUE;
            operator = INVALID_OP;
        }

        // Add a value to the first available slot in this
        // Calculation.
        public void pushValue(int value) {
            if (leftSide == INVALID_VALUE)
                leftSide = value;
            else
                rightSide = value;
        }

        public boolean hasOperator() { return operator != INVALID_OP; }
        public void setOperator(char operator) {
            // Only add the given operator if it's in OPERATORS
            if (OPERATORS.indexOf(operator) != -1)
                this.operator = operator;
        }

        // Solves this calculation, then resets it, then returns the
        // result.
        public int solve() {
            int result;

            // The user pushed the equals button before entering a
            // whole calculation, so we just use whatever number
            // they've entered so far.
            if (!ready()) {
                result = leftSide;
                reset();
                return result;
            }

            // Otherwise, we make the calculation.
            switch (operator) {
            case '+':
                result = leftSide + rightSide;
                break;
            case '-':
                result = leftSide - rightSide;
                break;
            case '\u00D7':
                result = leftSide * rightSide;
                break;
            default:
                // We shouldn't ever get to this point
                System.out.println("Unknown/invalid operator: " + operator);
                result = -1;
            }

            // Reset this calculation and return the result.
            reset();
            return result;
        }

        // Is this calculation ready to be solved?
        public boolean ready() {
            return leftSide != INVALID_VALUE
                && rightSide != INVALID_VALUE
                && operator != INVALID_OP;
        }

        public String toString() { return leftSide + " " + operator + " " + rightSide; }
    }
}
