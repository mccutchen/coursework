/*
 * File: Employee.java
 * 
 * Description: Model an employee with a first name, a last name and a
 * monthly salary.  Implements getters for instance variables, and an
 * earnings() method to calculate yearly earnings.  Also overrides the
 * Object.toString() method.
 * 
 * Name: Will McCutchen
 * 
 * UT EID: willm
 * 
 * Course Name: CS 313E
 * 
 * Unique Number: 56440
 * 
 * Date Created: 09/23/2007
 * 
 * Date Last Modified: 09/25/2007
 */

public class Employee {
    private String firstName;
    private String lastName;
    private double monthlySalary;
    
    public Employee(String firstName, String lastName, double monthlySalary) {
        this.firstName = firstName;
        this.lastName = lastName;
        
        // Only use the given salary if it is more than 0
        this.monthlySalary = Math.max(monthlySalary, 0);
    }
    
    // Calculate the yearly earnings based on the monthly salary
    public double earnings() {
        return getMonthlySalary() * 12;
    }
    
    // Getters
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public double getMonthlySalary() {
        return monthlySalary;
    }
    
    // Override toString() method.  Note, this returns a string with a line
    // break.
    public String toString() {
        return getFirstName() + " " + getLastName() +
            "\nAnnual salary: $" + earnings();
    }
}
