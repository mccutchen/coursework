/*
 * File: TestEmployee.java
 * 
 * Description: Tests Employee and Boss classes by reading employee
 * information from an input file and creating instances of Employee
 * and Boss.
 * 
 * Name: Will McCutchen
 * 
 * UT EID: willm
 * 
 * Course Name: CS 313E
 * 
 * Unique Number: 56440
 * 
 * Date Created: 09/23/2007
 * 
 * Date Last Modified: 09/25/2007
 */

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class TestEmployee {
    
    public static void main(String[] args) throws FileNotFoundException {
        File inputFile = new File("employee.txt");
        Scanner inputScanner = new Scanner(inputFile);

        // Each employee section starts with a number indicating
        // whether or not the employee is a boss or a normal employee.
        // So we loop through the file until we no longer get a
        // number.
        while(inputScanner.hasNextDouble()) {
            // If the first number in an employee block is a 1, it's a
            // boss.
            boolean isBoss = (inputScanner.nextDouble() == 1);

            // Create an Employee object (may also be a Boss object)
            // from the data in the next lines in the input file.
            Employee employee = parseEmployee(inputScanner, isBoss);

            // Print the employee to verify the data is correct.
            System.out.println(employee + "\n");
        }
    }

    // Parses employee information from the given input Scanner.  Each
    // bit of employee info is on a separate line, in this order:
    // first name, last name, monthly salary.  If the employee is a
    // boss (indicated by the isBoss flag), there is a line after
    // monthly salary for the boss's annual bonus.
    private static Employee parseEmployee(Scanner input, boolean isBoss) {
        // This data is the same for normal employees and bosses
        String firstName = input.next();
        String lastName = input.next();
        double monthlySalary = input.nextDouble();

        // If we're parsing a boss, get the next line and return a
        // Boss object
        if (isBoss) {
            double annualBonus = input.nextDouble();
            return new Boss(firstName, lastName, monthlySalary, annualBonus);
        }

        // Otherwise, just return an employee with the above data.
        else {
            return new Employee(firstName, lastName, monthlySalary);
        }
    }
}
