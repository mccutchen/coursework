/*
 * File: Boss.java
 * 
 * Description: Inherits from the Employee class, adding an annual
 * bonus instance variable and overriding the Employee.earnings()
 * method to account for the annual bonus.  Also overrides the
 * Employee.toString() method to indicate that this is a boss.
 * 
 * Name: Will McCutchen
 * 
 * UT EID: willm
 * 
 * Course Name: CS 313E
 * 
 * Unique Number: 56440
 * 
 * Date Created: 09/23/2007
 * 
 * Date Last Modified: 09/25/2007
 */

public class Boss extends Employee {
    private double annualBonus;

    public Boss(String firstName, String lastName, double monthlySalary, double annualBonus) {
        super(firstName, lastName, monthlySalary);
        
        // Only use the given annual bonus if it's more than 0
        this.annualBonus = Math.max(annualBonus, 0);
    }
    
    // Override earnings() method to add the annual bonus to the earnings.
    public double earnings() {
        return super.earnings() + annualBonus;
    }
    
    // Override toString() method to indicate that this employee is a boss.
    public String toString() {
        return "Boss: " + super.toString();
    }
}
