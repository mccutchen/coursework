import math, operator

"""Provides a function, least_common_multiple, which will calculate the
least common multiple of any two numbers 2 or greater.  All of the functions
in this module assume that their input, n, is 2 or greater."""

def divisors(n):
    """An iterator over the divisors of n."""
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i is 0:
            yield [i, n/i]

def is_prime(n):
    for a, b in divisors(n):
        # If n has any divisors, it is not prime
        return False
    return True

def get_prime_factors(n):
    """Returns a list of the prime factors of n.  Assumes n >= 2."""
    if is_prime(n):
        return [n]
    x, y = divisors(n).next()
    return get_prime_factors(x) + get_prime_factors(y)

def make_factor_map(factors):
    """Turns a list of prime factors into a map between each factor and
    the number of times it occurs."""
    factor_map = {}
    for factor in factors:
        factor_map[factor] = factor_map.get(factor, 0) + 1
    return factor_map

def least_common_multiple(a, b):
    """Calculate the least common multiple of a and b, which must both be
    2 or greater."""
    
    # Get the prime factor maps
    amap = make_factor_map(get_prime_factors(a))
    bmap = make_factor_map(get_prime_factors(b))
    
    # Collect each component into a list
    components = []
    for factor in set(amap.keys() + bmap.keys()):
        exponent = max(amap.get(factor, 0), bmap.get(factor, 0))
        components.append(factor**exponent)
    
    # Multiply each component together and return
    return int(reduce(operator.mul, components))


if __name__ == '__main__':
    import sys
    try:
        a, b = map(int, sys.argv[1:])
    except ValueError:
        print 'Usage: %s n m (where n and m are integers >= 2)' % __file__
        sys.exit(1)
    
    # Make sure we have valid input
    assert a >= 2 and b >= 2, 'Both %d and %d must be 2 or greater.' % (a, b)
    
    # Calculate the least common multiple and report the results
    lcm = least_common_multiple(a, b)
    print 'LCM of %d and %d: %d' % (a, b, lcm)
    print '(%d x %d, %d x %d)' % (a, lcm/a, b, lcm/b)
