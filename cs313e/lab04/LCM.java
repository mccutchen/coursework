/*
 * File: LCM.java
 * 
 * Description: Calculates the least common multiple for two integers,
 * which must be 2 or greater.  The user is prompted for the two
 * integers.  Uses a 2D array to store the table of prime factors for
 * each integer.
 *
 * Name: Will McCutchen
 * 
 * UT EID: willm
 * 
 * Course Name: CS 313E
 * 
 * Unique Number: 56440
 * 
 * Date Created: 10/15/2007
 * 
 * Date Last Modified: 10/17/2007
 */

import java.util.*;

public class LCM {

    // Prompt the user for two integers, which must be 2 or greater.
    // Find the least common multiple for the given integers.
    public static void main(String[] args) {
        // The two numbers for which to calculate the least common
        // multiple.
        int x, y;

        // If we were given command line args, use them (useful for
        // debugging).
        if (args.length == 2) {
            x = Integer.parseInt(args[0]);
            y = Integer.parseInt(args[1]);
        }

        // Otherwise, prompt the user for two integers.
        else {
            System.out.print("Enter two integers 2 or greater: ");
            Scanner inputScanner = new Scanner(System.in);
            x = inputScanner.nextInt();
            y = inputScanner.nextInt();
        }

        // Valid input.
        if (x >= 2 && y >= 2) {
            int lcm = calculate(x, y);
            System.out.println("Least common multiple of " + x + " and " + y + " is " + lcm);
        }

        // Invalid input.
        else {
            System.out.println("Both " + x + " and " + y + " must be 2 or greater.");
        }
    }

    
    // Public interface for the LCM class.  This method calculates the
    // least common multiple of its two integer arguments.  Both
    // arguments are assumed to be 2 or greater.
    public static int calculate(int x, int y) {
        // First, get the two tables of prime factors, as dictated by
        // the assignment.
        int[][] xfactors = getPrimeFactors(x);
        int[][] yfactors = getPrimeFactors(y);

        // This will hold our final least common multiple value.
        int lcm = 1;

        // First, loop through the table of prime factors for x.
        for (int[] factorAndExponent: xfactors) {
            // Get the factor and exponent for the current row.
            int factor = factorAndExponent[0];
            int exponent = factorAndExponent[1];

            // Get the exponent for this factor from the other factor
            // table, if it exists.  This will be 0, otherwise.
            int otherExponent = findExponentForFactor(factor, yfactors);

            // Multiply the current lcm by the factor raised to the
            // highest exponent found in either table.
            lcm *= Math.pow(factor, Math.max(exponent, otherExponent));
        }

        // Next, loop through the table of prime factors for y, to
        // make sure we include factors in y but not in x.
        for (int[] factorAndExponent: yfactors) {
            // Get the factor and exponent for the current row.
            int factor = factorAndExponent[0];
            int exponent = factorAndExponent[1];

            // Get the exponent for this factor from the other factor
            // table, if it exists.
            int otherExponent = findExponentForFactor(factor, xfactors);

            // We only need to multiply the lcm by this factor raised
            // to its exponent from this factor table if this factor
            // was not found in the other factor table (which means
            // otherExponent would be 0).  This way, we only include
            // factors in this table but missing from the other table,
            // which we already processed.
            if (otherExponent == 0)
                lcm *= Math.pow(factor, exponent);
        }
        
        return lcm;
    }

    // Create a 2D array of prime factors for the given integer.
    private static int[][] getPrimeFactors(int n) {
        return makeFactorTable(getPrimeFactorArray(n));
    }

    // Recursive method that creates an array containing the prime
    // factors of the given integer.  The resulting array is turned
    // into a 2D array by the makeFactorTable method.  For example,
    // given 120 as input, returns [2,2,2,3,5].
    private static int[] getPrimeFactorArray(int n) {
        if (isPrime(n)) {
            // If n is prime, return a single-element array containing
            // only n.
            int[] result = {n};
            return result;
        }
        else {
            // Otherwise, we need to keep on factoring n.  Return an
            // array that is the result of concatenating the results
            // of further factoring.
            int[] divisors = findDivisors(n);
            return concat(getPrimeFactorArray(divisors[0]), getPrimeFactorArray(divisors[1]));
        }
    }

    // Returns a 2D array of [factor, count] rows that is the result
    // of counting each factor in the given array of factors.  For
    // example, given input of [2,2,2,3,5,5] returns
    // [[2,3],[3,1],[5,2]]
    private static int[][] makeFactorTable(int[] factors) {
        // First, build a map of factors to their number of occurences
        HashMap<Integer, Integer> factorMap = new HashMap<Integer, Integer>();
        for (int i: factors) {
            if (factorMap.containsKey(i)) {
                // If the key is already in the map, just increment the count
                factorMap.put(i, factorMap.get(i) + 1);
            } else {
                // Otherwise, add the key and a count of 1
                factorMap.put(i, 1);
            }
        }

        // Now, turn the HashMap we just made into a 2D array
        return hashMapToArray2D(factorMap);
    }

    // Finds two divisors of the given integer n.  Assumes n to be
    // positive and non-prime (thus, 4 or greater). For example, given
    // 20 as input returns [2, 10].
    private static int[] findDivisors(int n) {
        for (int i = 2; i <= Math.sqrt(n); i++) {
            // We found a number i that will go into n evenly.
            if (n % i == 0) {
                // Return i and n/i, the two factors.
                int[] divisors = {i, n/i};
                return divisors;
            }
        }
        throw new RuntimeException("No divisors found for " + n + ", even though it should not be prime.");
    }

    // Determines whether or not n is a prime number.  Assumes that n
    // is 2 or greater.
    private static boolean isPrime(int n) {
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0)
                // We found a number i that divides into n evenly, so
                // n is not a prime number.
                return false;
        }

        // No clean divisors found, so n must be prime.
        return true;
    }

    // If the given factor is in the given factor table (from the
    // first column), returns its exponent (from the second column).
    // Otherwise, returns 0.
    private static int findExponentForFactor(int factor, int[][] factorTable) {
        for (int[] factorAndExponent: factorTable)
            // Did we find the factor we're looking for?
            if (factorAndExponent[0] == factor)
                return factorAndExponent[1];
        // We didn't find what we were looking, so we return 0.
        return 0;
    }


    /* ========================================================================
     * Utility methods
     * ===================================================================== */

    // Concatenates two integer arrays.
    private static int[] concat(int[] a, int[] b) {
        int[] c = new int[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }

    // Transforms a HashMap with integer keys and values into a 2D
    // array with each row being a key and value from the given map.
    private static int[][] hashMapToArray2D(HashMap<Integer, Integer> map) {
        int[][] table = new int[map.size()][2];
        int i = 0;
        for (int key: map.keySet()) {
            table[i][0] = key;
            table[i][1] = map.get(key);
            i++;
        }
        return table;
    }
}
