def make_grid(input):
    """Returns a grid (a list of lists) taken from the given input file."""
    grid = [line.strip().split() for line in file(input)]
    return grid[1:] # drop the first line, which we don't need

def find_word(word, grid):
    """Find the given word in the given grid of letters. Returns
    coordinates (row, column) of word's starting position if found,
    None otherwise."""    

    # First, search for words running horizontally
    hcoords = find_word_horizontally(word, grid)
    if hcoords is not None:
        return hcoords

    # Next, transpose the grid and search again for words running
    # vertically
    vcoords = find_word_horizontally(word, zip(*grid))
    if vcoords:
        # Switch the coordinates that we have to compensate for the
        # transposed grid
        return tuple(reversed(vcoords))

    # No word found
    return None
    
def find_word_horizontally(word, grid):
    """Does the actual work of finding the given word in the given grid of
    letters.  This only searches for words running horizontally.  To
    find vertical words, transpose the grid you pass in and the
    resulting coordinates you are returned."""
    for rownum, row in enumerate(grid):
        # Is the word in this row?
        colnum = ''.join(row).find(word)
        if colnum > -1:
            return rownum, colnum
    return None

def main(gridpath, wordpath):
    """Check to see the word on each line in the wordpath file is found in
    the grid of letters in the gridpath file.  Report results to
    stdout."""
    grid = make_grid(gridpath)
    for word in (word.strip() for word in file(wordpath)):
        print '%s:\t' % word,
        coords = find_word(word, grid)
        if coords is not None:
            print 'row %d, col %d' % coords
        else:
            print 'not found'
    return

if __name__ == '__main__':
    gridpath = 'grid.txt'
    wordpath = 'words.txt'
    main(gridpath, wordpath)
