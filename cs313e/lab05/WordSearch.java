/*
 * File: WordSearch.java
 * 
 * Description: A program that takes two filenames on the command
 * line, for a file containing a word search puzzle grid and a file
 * containing a set of words to find the grid.  For each word in the
 * second file, print either the row and column of its first character
 * in the grid, or "word not found" if the word does not appear in the
 * grid.
 *
 * Name: Will McCutchen
 * 
 * UT EID: willm
 * 
 * Course Name: CS 313E
 * 
 * Unique Number: 56440
 * 
 * Date Created: 10/22/2007
 * 
 * Date Last Modified: 10/25/2007
 */


import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;


/**
 * Public interface into the WordSearch program.  Provides a main
 * method that takes two arguments, the path to the grid file and the
 * path to the words file.  Creates a CharGrid from the grid file,
 * searches it for each word in the word file, and reports the
 * results.
 **/
public class WordSearch {

    public static void main(String[] args) throws FileNotFoundException {
        
        // Make sure we got the correct number of command line arguments
        if (args.length != 2) {
            System.out.println("Two arguments required, for the path to " +
                               "the grid file and the words file.");
            System.exit(1);
        }
        
        // Get the paths to the files to use from the command line
        String gridPath = args[0];
        String wordsPath = args[1];
        
        // Create the character grid from the specified file
        CharGrid grid = CharGrid.fromFile(gridPath);

        // Search the grid for each word in the words file.
        Scanner wordScanner = new Scanner(new File(wordsPath));
        while(wordScanner.hasNext()) {
            String word = wordScanner.next();
            System.out.print(word + ":\t");
            GridCoords coords = grid.findWord(word);
            if (coords != null)
                System.out.println(coords);
            else
                System.out.println("word not found");
        }
    }
}


/**
 * CharGrid represents a grid of characters as a 2D array.  A CharGrid
 * can only be instantiated by the fromFile static factory-style
 * method.  A CharGrid provides only one public method, findWord,
 * which will return a GridCoords object if the word is found or null
 * if the word was not found.
 **/
class CharGrid {
    private int rows;
    private int cols;
    private char[][] data;

    private CharGrid(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        data = new char[rows][cols];
    }

    // Searches for the given word in this grid.  If found, returns a
    // GridCoords object representing the coordinates of the first
    // letter of the word.  Otherwise, returns null.
    public GridCoords findWord(String word) {
        // First, search this grid for the word running horizontally
        GridCoords coords = findWordHorizontally(word);
        
        // Did we find the word?  If so, return its starting
        // coordinates.
        if (coords != null)
            return coords;
        
        // Otherwise, transpose this grid and search horizontally
        // again (which is the same as searching vertically)
        coords = transpose().findWordHorizontally(word);
        
        // Did we find the word running vertically?  If so, swap the
        // given coordinates to account for the transposed grid.
        if (coords != null)
            return new GridCoords(coords.col, coords.row);
        
        // We didn't find any coords, so we just return null.
        return null;
    }

    // Searches for the given word running either forward or backward
    // horizontally in this grid.  If found, returns the GridCoords of
    // its first letter's starting position.  Otherwise, returns null.
    private GridCoords findWordHorizontally(String word) {
        // Loop through each row looking for the word.
        for (int i = 0; i < rows; i++) {
            // Convert this row into a String and see if it contains
            // the word.
            String rowString = new String(data[i]);
            int j = rowString.indexOf(word);
            if (j > -1)
                // We found it.
                return new GridCoords(i, j);
            
            // We didn't find it running forwards.  Check backwards.
            j = rowString.indexOf(StringUtils.reverse(word));
            if (j > -1)
                // We found it.  Adjust the coords to compensate for
                // reversing the word.
                return new GridCoords(i, j + word.length());
        }
        
        // We didn't find the word.
        return null;
    }

    // Transposes this grid, which is helpful if you want to search
    // for words running vertically.
    private CharGrid transpose() {
        CharGrid result = new CharGrid(cols, rows);
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                result.data[j][i] = data[i][j];
        return result;
    }

    // Creates a new CharGrid object from the file at the given path.
    public static CharGrid fromFile(String path) throws FileNotFoundException {
        // Create a scanner to read the grid from gridPath
        Scanner gridScanner = new Scanner(new File(path));

        // Create a scanner to read the size of the grid from the
        // first row of the grid file.
        Scanner sizeScanner = new Scanner(gridScanner.nextLine());
        int rows = sizeScanner.nextInt();
        int cols = sizeScanner.nextInt();

        // Create the new grid with the dimensions from the grid file
        CharGrid grid = new CharGrid(rows, cols);

        // Loop through each row and column in the grid file and add
        // each char to our new grid.
        Scanner rowScanner;
        for (int i = 0; i < rows; i++) {
            rowScanner = new Scanner(gridScanner.nextLine());
            for (int j = 0; j < cols; j++)
                grid.data[i][j] = rowScanner.next().charAt(0);
        }

        return grid;
    }
}


/**
 * Represents the coordinates of a given letter on a CharGrid in terms
 * of row and column.  Returned from the various findWord* methods in
 * CharGrid.
 **/
class GridCoords {
    public int row;
    public int col;

    public GridCoords(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public String toString() {
        return "row " + row + ", column " + col;
    }
}


/**
 * Provides a utility method for reversing a string.  Naive
 * impementation.
 **/
class StringUtils {
    public static String reverse(String src) {
        String dest = "";
        for (int i = src.length() - 1; i >= 0; i--) {
            dest += src.charAt(i);
        }
        return dest;
    }
}
