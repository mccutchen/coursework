Lab 5 - CS 313e
Fall 2007
Due Wednesday, October 31, 2007 at 9 pm

Goal: More practice with file processing and arrays

For this project, you will write a program that takes two filenames on
the command line. The first file will contain a word search puzzle
grid, and the second file will contain the words you must find in the
grid.

The first line in file 1 will be the number of rows and columns in the
grid. The rest of the lines will be rows of letters, separated by
blank space. Here is an example file:

5 6
a f e i o p
h e l l o s
e a p e t r
r a t t r a
e i o o p s

The second file will contain the words you must search for, one per
line. For example:

oops
ape
hello
here
cantaloupe

If the words in the second file appear in the grid, they will either
appear in a horizontal or vertical line.

For each word in the second file, you must print either the row and
column of its first character in the grid, or "word not found" if the
word does not appear in the grid. For the example files above, the
output to the screen would be:

oops: row 4, column 2
ape: row 2, column 1
hello: row 1, column 0
here: row 1, column 0
cantaloupe: word not found

If a word appears more than once, you can identify any occurrence. You
do not need to find all occurrences.

You will be graded on:
    * your design - did you appropriately break the problem into
      methods
    * use of whitespace and comments to make your program readable
    * correctness
    * efficiency

Submit your file WordSearch.java by 9 pm on the due date.

